<?php
/**
 * @file
 *    A module for Drush that extends the Provision module to allow it to work with Drupal sites that are not owned by
 *    the Aegir user account.
 *
 *    To do this, this module temporarily changes the owner of a Drupal site over to the Aegir user account before an
 *    operation, and then restores ownership at the end of the operation.
 *
 * @author
 *    Guy Paddock (guy.paddock@redbottledesign.com)
 */

  /**
   * Name of the Drush context option that stores the username of the user who originally owned a site folder before
   * Aegir took ownership of it.
   *
   * @var   string
   */
  define('DOP_OPTION_SITE_OWNER', 'owner_user');

  /**
   * The Drush command for Provision operations.
   *
   * @var string
   */
  define('DOP_COMMAND_PROVISION', 'provision');

  /**
   * Implementation of hook_drush_init().
   *
   * When in the "site" context, this callback is used to handle the "provision migrate" command specially. This logic
   * is invoked here rather than in a pre_ hook because it has to happen before all of Provision's own pre_ hooks.
   *
   * TODO: Determine if there is a way to prioritize certain "pre_" hooks over others so that this isn't necessary.
   *
   * @param $url
   *        When in the site context, the URL (i.e. "example.com", "aegir.redbottledesign.com") of the site that is
   *        being modified.
   */
  function site_owner_preservation_drush_init($url = NULL) {
    if (!empty($url) && defined('PROVISION_CONTEXT_SITE') && PROVISION_CONTEXT_SITE) {
      $command = drush_get_command();
      $command = explode(" ", $command['command']);

      // Only work with Provision commands.
      if (((count($command) > 1) && ($command[0] == DOP_COMMAND_PROVISION)) || // Provision 0.3 -- "provision migrate"
          (preg_match("/^" . DOP_COMMAND_PROVISION . "-/", $command[0]))) {    // Provision 0.4 -- "provision-migrate"
        _drush_site_owner_preservation_make_aegir_site_owner($url);
      }
    }
  }

  /**
   * Implementation of hook_drush_exit().
   *
   * When in the "site" context, this callback makes sure that a site folder that Aegir has taken ownership of, if
   * any, is returned to its rightful owner before the end of a task.
   *
   * This logic is invoked here rather than in post_ hooks because it has to happen after the drushrc.php file is
   * written out.
   *
   * @param $url
   *        When in the site context, the URL (i.e. "example.com", "aegir.redbottledesign.com") of the site that was
   *        being modified.
   */
  function site_owner_preservation_drush_exit($url = NULL) {
    if (($url != NULL) && defined('PROVISION_CONTEXT_SITE') && PROVISION_CONTEXT_SITE) {
      _drush_site_owner_preservation_restore_site_owner($url);
    }
  }

  /**
   * Have Aegir take ownership of the site at the specified URL.
   *
   * @param $url
   *        The URL (i.e. "example.com", "aegir.redbottledesign.com") of the site that Aegir should take ownership of.
   *
   * @param $platform
   *        An optional parameter that specifies the path to the platform that is hosting the site. If not specified,
   *        the default is the platform in the current working directory (set by Aegir).
   */
  function _drush_site_owner_preservation_make_aegir_site_owner($url, $platform = NULL) {
    // Based on code elsewhere in Provision, the current working directory will be inside the platform folder
    // (i.e. /var/aegir/platforms/drupal-6.x/)
    drush_log(dt('Site owner preservation: At start, the current working directory is %path.', array('%path' => getcwd())));

    $siteFolder = _drush_site_owner_preservation_get_site_folder($url, $platform);
    $lastUser   = drush_get_option(DOP_OPTION_SITE_OWNER, NULL);

    if (!file_exists($siteFolder)) {
      drush_log(
        dt("Site owner preservation: The site folder %path doesn't exist; no owner change is necessary.",
        array('%path' => $siteFolder)));
    }
    else if ($lastUser != NULL) {
      drush_log(
        dt('Site owner preservation: Ownership of the site folder %path has already been changed once during this session '.
           '(to %user); no owner change is necessary.',
        array('%path' => $siteFolder, '%user' => $lastUser)));
    }
    else {
      $siteOwner  = provision_path_owner($siteFolder);
      $aegirUser  = drush_get_option('script_user');
      $webGroup   = drush_get_option('web_group');

      drush_log(dt('Site owner preservation: The current owner of the site is %owner.', array('%owner' => $siteOwner)));

      ownership_utils_sudo_chown_logged(
        $aegirUser,
        $webGroup,
        $siteFolder,
        TRUE,
        'Site owner preservation: Temporarily changed owner of %path to %user.',
        "Site owner preservation: Failed to change site owner to %user");

      // Store the owner's username for later.
      drush_set_option(DOP_OPTION_SITE_OWNER, $siteOwner, 'process');
    }
  }

  /**
   * Have Aegir restore ownership of the site at the specified URL to its original owner, if ownership of the site
   * was previously taken by _drush_site_owner_preservation_make_aegir_site_owner().
   *
   * @param $url
   *        The URL (i.e. "example.com", "aegir.redbottledesign.com") of the site that Aegir should restore
   *        ownership for.
   *
   * @param $platform
   *        An optional parameter that specifies the path to the platform that is hosting the site. If not specified,
   *        the default is the platform in the current working directory (set by Aegir).
   */
  function _drush_site_owner_preservation_restore_site_owner($url, $platform = NULL) {
    // Based on code elsewhere in Provision, the current working directory will be inside the platform folder
    // (i.e. /var/aegir/platforms/drupal-6.x/)
    drush_log(dt('Site owner preservation: At end, the current working directory is %path.', array('%path' => getcwd())));

    $siteFolder = _drush_site_owner_preservation_get_site_folder($url, $platform);

    if (file_exists($siteFolder)) {
      $siteOwner  = drush_get_option(DOP_OPTION_SITE_OWNER, NULL);

      if ($siteOwner == NULL) {
        drush_log(
          dt('Site owner preservation: The username of the original site owner was not stored; no owner change is '.
             'necessary.'));
      }
      else {
        $webGroup  = drush_get_option('web_group');

        // Keep the site folder safe from prying eyes
        provision_path(
          'chmod',
          $siteFolder,
          0750,
          "Site owner preservation: Changed permissions of <code>@path</code> to @confirm",
          "Site owner preservation: Could not change permissions <code>@path</code> to @confirm",
          'DRUSH_PERM_ERROR');

        // Change permission on all *files* (not directories) in the site to 640, so they're not shell-executable
        ownership_utils_recursive_chmod_files($siteFolder, 0640);

        ownership_utils_sudo_chown_logged(
          $siteOwner,
          $webGroup,
          $siteFolder,
          TRUE,
          'Site owner preservation: Restored owner of %path to %user.',
          'Site owner preservation: Failed to restore ownership of site to %user.');

        // Remove stored username.
        drush_unset_option(DOP_OPTION_SITE_OWNER, 'process');
      }
    }
  }

  /**
   * Return the path to the site with the specified URL.
   *
   * @param $url
   *        The URL of the site for which a path will be returned.
   *
   * @param $platform
   *        An optional parameter that specifies the path to the platform that is hosting the site. If not specified,
   *        the default is the platform in the current working directory (set by Aegir).
   */
  function _drush_site_owner_preservation_get_site_folder($url, $platform = NULL) {
    $siteFolder = "sites/$url";

    // Prefix with path to the platform.
    if (!empty($platform)) {
      $siteFolder = $platform . '/' . $siteFolder;
    }

    return $siteFolder;
  }