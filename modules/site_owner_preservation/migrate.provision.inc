<?php
/**
 * @file
 *    Hooks for extending Provision's "migrate" command to preseve ownership of Drupal sites.
 *
 * @author
 *    Guy Paddock (guy.paddock@redbottledesign.com)
 */

  /**
   * Implementation of hook_post_provision_backup().
   *
   * This function ensures that a site that has been migrated from one platform to another is returned to its rightful
   * owner before the end of the migration.
   *
   * This has to be done here rather than in site_owner_preservation_drush_exit() because it's the only opportunity
   * we have to get the full path to the new platform. The migration starts and ends with the current working directory
   * in the *old* platform folder, so the normal method won't work.
   *
   * @param $url
   *        The URL of the site (i.e. "example.com", "aegir.redbottledesign.com") being migrated.
   *        This will be the same name as the folder for the site under the "sites/" folder.
   *
   * @param $platform
   *        The path to the platform that the site has been migrated to.
   */
  function drush_site_owner_preservation_post_provision_migrate($url, $platform) {
    _drush_site_owner_preservation_restore_site_owner($url, $platform);
  }
