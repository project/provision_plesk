<?php
/**
 * @file
 *  Functions for locking-down the modules that are loaded when a Drupal install is being bootstrapped with Drush
 *  by Aegir.
 *
 *  NOTE: In order for this module to work properly, the included patch MUST be applied to module_list() in
 *  includes/module.inc.
 *
 * @author  Guy Paddock (guy.paddock@redbottledesign.com)
 */
  /**
   * Determine whether it is "safe" to load the specified module file.
   *
   * Module safety is only a consideration when a Drupal install is being bootstrapped by Drush while running under
   * the user account used by Aegir. This is because a module can contain malicious custom code that can exploit the
   * permissions of the Aegir account, including the ability to read and write platform and site settings files, and
   * the ability to take ownership of sites, folders, and files.
   *
   * A module is considered "safe" to load if either of the following conditions are TRUE:
   *  * The Drupal install is not being bootstrapped while running under the Aegir user account, OR
   *  * The module is a shared, system-wide Drupal module
   *    (either a module provided by core, or a module installed for all sites to user).
   *
   * With these restrictions in place, it should be impossible for a normal user to write malicious code that will be
   * executed while the code of a Drupal site is being executed under the Aegir account.
   *
   * @param   $moduleFile
   *          The filename of the module file for which safety is to be determined
   *          (for example, "modules/block/block.module").
   *
   * @return  TRUE if it is safe to load the specified module under current conditions, or FALSE if the module would
   *          present a security risk and should not be loaded.
   */
  function hardened_modules_module_is_safe($moduleFile) {
    static $aegirUser   = NULL;
    static $currentUser = NULL;

    // Initialize static variables on the first call.
    if (($aegirUser == NULL) || ($currentUser == NULL)) {
      $aegirUser    = drush_get_option('script_user');
      $currentUser  = exec('whoami');
    }

    // Only restrict which modules can load when running under the Aegir user account
    if ($aegirUser != $currentUser) {
      $isSafe = TRUE;
      drupal_set_message(
        dt('Hardened Modules: !module is SAFE to load (no load restrictions when running as !user).',
           array(
             '!module'  => $moduleFile,
             '!user'    => $currentUser)));
    }
    else {
      $isSafe = _hardened_modules_is_shared_module($moduleFile);

      if ($isSafe) {
        drupal_set_message(
          dt('Hardened Modules: !module is SAFE to load (shared module).',
             array('!module' => $moduleFile)));
      }
      else {
        drupal_set_message(
          dt('Hardened Modules: !module is NOT SAFE to load (only shared modules are loaded when running as !user).',
             array(
               '!module'  => $moduleFile,
               '!user'    => $currentUser)));
      }
    }

    return $isSafe;
  }

  /**
   * Internal function for determining whether the specified module file belongs to a shared module (i.e. a Drupal code
   * module, or a module that has been installed on the platform for use by multiple Drupal sites).
   *
   *
   * @param   $moduleFile
   *          The filename of the module file that will be examined (for example, "modules/block/block.module").
   *
   * @return  TRUE if the the specified module file belongs to a shared module, FALSE otherwise.
   */
  function _hardened_modules_is_shared_module($moduleFile) {
    $modulePath = realpath($moduleFile);

    $platformPath       = realpath(drush_get_option('publish_path'));
    $sharedModulePaths  =
      array(
        "$platformPath/modules",            // Core modules
        "$platformPath/sites/all/modules",  // Shared, contributed modules.
      );

    $isShared = FALSE;

    foreach ($sharedModulePaths as $path) {
      if (preg_match("!^{$path}!", $modulePath) === 1) {
        $isShared = TRUE;
        break;
      }
    }

    return $isShared;
  }