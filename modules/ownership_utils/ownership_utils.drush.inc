<?php
/**
 * @file
 *    Utility functions for dealing with the ownership of files or folders.
 *
 * @author
 *    Guy Paddock (guy.paddock@redbottledesign.com)
 */
  /**
   * Change the owner of the specified file or folder to the specified user or group using the appropriate
   * <code>chown</code> command with <code>sudo (i.e. <code>sudo aegir_chown</code> or <code>sudo chown</code>), but
   * writing the necessary messages to the drush log upon success or failure (like provision_path()).
   *
   * Messages should NOT be run through dt() before being passed in. The tokens available to each message are:
   *  - %path   - The path whose owner is attempting to be changed.
   *  - %user   - The user that is to be made the owner of the path.
   *  - %group  - The group that is to be made the owner of the path.
   *
   * @param   $username
   *          The name of the UNIX account which should become the new owner of the file or folder.
   *
   * @param   $group
   *          The name of the UNIX group which should become the new owner of the file or folder.
   *
   * @param   $file
   *          The path to the file or folder whose ownership will be modified.
   *
   * @param   $recursive
   *          If <code>$file</code> specifies the path to a folder, this parameter is used to indicate whether or
   *          not the operation should be recursive. The default is <code>FALSE</code>.
   *
   * @param   $success_msg
   *          An optional parameter specify the message to display upon success.
   *          The default is not to display a message.
   *
   * @param   $failure_msg
   *          An optional parameter specify the message to display upon failure.
   *          The default is not to display a message.
   */
  function ownership_utils_sudo_chown_logged($username, $group, $file, $recursive = FALSE, $success_msg = NULL,
                                             $failure_msg = NULL) {
    if (ownership_utils_sudo_chown($username, $group, $file, $recursive)) {
      if (!empty($success_msg)) {
        drush_log(dt($success_msg, array('%path' => $file, '%user' => $username, '%group' => $group), 'message'));
      }
    }
    else {
      if (!empty($failure_msg)) {
        drush_set_error(
          'DRUSH_PERM_ERROR',
          dt($failure_msg, array('%path' => $file, '%user' => $username, '%group' => $group), 'message'));
      }
    }
  }

  /**
   * Change the owner of the specified file or folder to the specified user or group using the appropriate
   * <code>chown</code> command with <code>sudo (i.e. <code>sudo aegir_chown</code> or <code>sudo chown</code>).
   *
   * @param   $username
   *          The name of the UNIX account which should become the new owner of the file or folder.
   *
   * @param   $group
   *          The name of the UNIX group which should become the new owner of the file or folder.
   *
   * @param   $file
   *          The path to the file or folder whose ownership will be modified.
   *
   * @param   $recursive
   *          If <code>$file</code> specifies the path to a folder, this parameter is used to indicate whether or
   *          not the operation should be recursive. The default is <code>FALSE</code>.
   *
   * @return  <code>TRUE</code> if the <code>chown</code> command succeeded, or <code>FALSE</code> if it failed.
   */
  function ownership_utils_sudo_chown($username, $group, $file, $recursive = FALSE) {
    $chownCommand = _ownership_utils_determine_chown_command();

    if ($chownCommand == NULL) {
      $returnVal  = FALSE;
    }
    else {
      $output   = ''; // Unused, but required for the call.
      $result   = 0;
      $command  = "sudo $chownCommand";

      if ($recursive)
        $command .= ' -R';

      @exec("$command '$username:$group' '$file'", $output, $result);

      $returnVal = ($result == 0);
    }

    return $returnVal;
  }

  /**
   * Walk the specified folder path, using Provision's provision_path() method to recursively change the
   * mode / permissions on all files, or files with a specified file name.
   *
   * For example, consider the following directory structure:
   * <pre>
   *  parent
   *    child
   *      file1
   *      file2
   *    file1
   * </pre>
   *
   * If <code>$path</code> was "parent", <code>$mode</code> was <code>0644</code>, and <code>$desiredFileName</code> was
   * not provided, then all files would be changed to mode <code>0644</code>. On the other hand, if
   * <code>$desiredFileName</code> was "file1", then only the modes on "parent/file1" and "parent/child/file1" would
   * change to <code>0644</code>.
   *
   * @param $path
   *        The path that should be searched for files.
   *
   * @param $mode
   *        The new mode, in octal, to set on all matching files. Remember that 2770 would be 02770 in octal.
   *
   * @param $desiredFileName
   *        An optional parameter specifying the specific name of the files on which the mode should be changed.
   *        If not provided, the mode of all files within <code>$path</code> will be changed to <code>$mode</code>.
   */
  function ownership_utils_recursive_chmod_files($path, $mode, $desiredFileName = NULL) {
    if (file_exists($path) && is_dir($path)) {
      foreach (scandir($path) as $file) {
        if (!in_array($file, array('.', '..'))) {
          $filePath = "$path/$file";

          if (is_file($filePath)) {
            // Either apply to all files (if no specific filename was provided), or apply to only files with a
            // specific name.
            if (empty($desiredFileName) || ($file == $desiredFileName)) {
              provision_path(
                'chmod',
                $filePath,
                $mode,
                "Changed permissions of @path to @confirm",
                "Could not change permissions on @path to @confirm",
                'DRUSH_PERM_ERROR');
            }
          }

          else if (is_dir($filePath)) {
            ownership_utils_recursive_chmod_files($filePath, $mode, $desiredFileName);
          }
        }
      }
    }
  }

  /**
   * Determines the appropriate <code>chown</code> command for the current environment, preferring
   * <code>aegir_chown</code> over standard <code>chown</code> for security reasons.
   */
  function _ownership_utils_determine_chown_command() {
    $acceptableCommands = array(
      'aegir_chown',
      'aegir_chown.php',  // In case the user didn't read the directions...
      'chown');

    $acceptablePaths    = array(
      '/usr/local/sbin',
      '/usr/sbin',
      '/sbin',
      '/usr/local/bin',
      '/usr/bin',
      '/bin');

    $command = NULL;

    foreach ($acceptableCommands as $currentCommand) {
      foreach ($acceptablePaths as $currentPath) {
        $currentCommandPath = "$currentPath/$currentCommand";

        if (file_exists($currentCommandPath)) {
          $command  = $currentCommandPath;
          break 2;
        }
      }
    }

    return $command;
  }