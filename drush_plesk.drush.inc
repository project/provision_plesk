<?php
/**
 * @file
 *   Drush commands for dealing with Drupal sites for user accounts and web spaces that are maintained by
 *   Parallels Plesk Server Administrator.
 *
 * @author
 *   Guy Paddock (guy.paddock@redbottledesign.com)
 */
require_once('drush_plesk.db.inc');
require_once('drush_plesk.utilities.inc');

require_once('drush_plesk.reset-pf-perm.inc');
require_once('drush_plesk.setup-drupal.inc');
require_once('drush_plesk.discover-db.inc');

define('DP_COMMAND_FIX_PERM',     'plesk-reset-pf-perm');
define('DP_COMMAND_MAKE_DOCROOT', 'plesk-setup-drupal');
define('DP_COMMAND_DISCOVER_DB',  'plesk-discover-db');

/**
 * Implementation of hook_drush_command().
 *
 * Specifies which commands this drush module makes available.
 *
 * @see drush_parse_command() for a list of recognized keys.
 *
 * @return
 *   An associative array describing the command(s).
 */
function drush_plesk_drush_command() {
  $items = array(
    DP_COMMAND_FIX_PERM => array(
      'description' => "Resets the permissions on a Drupal platform/install folder (requires chown privileges).",
      'bootstrap'   => DRUSH_BOOTSTRAP_DRUPAL_ROOT,
    ),

    DP_COMMAND_MAKE_DOCROOT => array(
      'description' =>
        "Initializes a client's document root folder with the necessary files and symlinks to run a Drupal site from " .
        "a shared Drupal platform/install (requires chown privileges).",

      'arguments'   => array(
        'document root' =>
          "The full path to the target client's document root folder (i.e. \"httpdocs\" or \"public_html\")."
      ),

      'bootstrap'   => DRUSH_BOOTSTRAP_DRUPAL_SITE,
    ),

    DP_COMMAND_DISCOVER_DB  => array(
      'description' =>
        "Updates Plesk's database with information about the location and ownership of the current site's database, ".
        "so that it can be administered and monitored from within Plesk.",

      'bootstrap'   => DRUSH_BOOTSTRAP_DRUPAL_SITE,
    ),
  );

  return $items;
}

/**
 * Implementation of hook_drush_help().
 *
 *
 * @param
 *   A string with the help section (prepend with 'drush:')
 *
 * @return
 *   A string with the help text for your command.
 */
function drush_plesk_drush_help($section) {
  $helpText = "";

  $section = preg_replace("/^drush:(.*)$/", "$1", $section, 1);

  switch ($section) {
    case DP_COMMAND_FIX_PERM:
      $helpText =
        dt("Reset all of the permissions and ownership of the current Drupal install to default values appropriate ".
           "for your Aegir and Plesk environment. This is useful if the current permissions are causing clients to ".
           "have trouble accessing their Drupal sites.\n\n" .

           "This command requires Aegir to have sudo access for the \"aegir_chown\" or \"chown\" command.\n\n".

           "NOTE: This will cause all sites under the sites/ folder to lose their owners. After running this ".
           "command, you will need to restore appropriate ownership to clients.");
      break;

    case DP_COMMAND_MAKE_DOCROOT:
      $helpText =
        dt("Set-up the specified client's document root folder to access the current Drupal site. This is done by ".
           "means of symlinks to the shared Drupal platform/install; the Drupal site remains in its current location ".
           "to allow it to be maintained by Aegir.\n\n" .

           "This command requires Aegir to have sudo access for the \"aegir_chown\" or \"chown\" command, because it ".
           "must temporarily take ownership of the document root, the user's home folder, and the Drupal site folder ".
           "while it runs.");
        break;

    case DP_COMMAND_DISCOVER_DB:
      $helpText =
        dt("Inform Plesk about the database that corresponds to the current Drupal site, so that the database is ".
           "associated with the appropriate client's account and can be managed and monitored within Plesk. This is ".
           "useful if Aegir automatically created the database for the site (as is the typical case) because it ".
           "allows you to leverage the additional quota and reporting tools Plesk offers, while freeing you from ".
           "having to create each database and user account manually for each site.");
        break;
  }

  return $helpText;
}