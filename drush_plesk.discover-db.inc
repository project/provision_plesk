<?php
/**
 * @file
 *  Logic for the "discover-db" command of the Drush Plesk module.
 *
 * @author
 *   Guy Paddock (guy.paddock@redbottledesign.com)
 */

  /**
   * Implementation of hook_drush_plesk_discover_db().
   *
   * Ensures that database settings are available for the current site.
   */
  function drush_drush_plesk_plesk_discover_db_validate() {
    drush_plesk_db_connect_to_master_database();

    // Unlikely to happen...
    if ((drush_get_option('db_name', NULL, 'site') == NULL) || (drush_get_option('db_user', NULL, 'site') == NULL)) {
      drush_set_error(dt('The current site either has incorrect database settings or does not have a database.'));
    }

    // Make sure Plesk is actually installed
    elseif (!_provision_mysql_database_exists(DP_DD_PLESK_DATABASE)) {
      drush_set_error(
        dt('The Plesk database (%psa_db) does not appear to exist on this machine.',
           array('%psa_db' => DP_DD_PLESK_DATABASE)));
    }
  }

  /**
   * Implementation of hook_drush_plesk_discover_db().
   *
   * Updates Plesk's database with information about the location and ownership of the current site's database,
   * so that it can be administered and monitored from within Plesk.
   */
  function drush_drush_plesk_plesk_discover_db() {
    $siteUrl          = drush_get_option('site_url');

    $databaseName     = drush_get_option('db_name', NULL, 'site');
    $databaseUser     = drush_get_option('db_user', NULL, 'site');
    $databasePassword = drush_get_option('db_passwd', NULL, 'site');

    $pleskDbId        = drush_plesk_db_get_or_insert_database_info($siteUrl, $databaseName);

    if ($pleskDbId !== NULL) {
      $pleskDbUserId  =
        drush_plesk_db_get_or_insert_database_user($pleskDbId, $databaseUser, $databasePassword);

      if ($pleskDbUserId !== NULL) {
        drush_plesk_db_update_database_default_user($pleskDbId, $pleskDbUserId);
      }
    }

    provision_db_close();

    if (!drush_get_error()) {
      drush_log(
        dt("\nThe Plesk database has been updated with information about the database for %site.",
           array('%site' => $siteUrl)),
        'success');
    }
  }

