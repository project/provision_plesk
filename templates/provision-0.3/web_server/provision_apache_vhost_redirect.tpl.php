<?php
  drush_plesk_get_plesk_info_for_template(
                                    $site_url,
                                    $ipAddress,
                                    $isPlesk,
                                    $pleskVhostPath,
                                    $publish_path,
                                    $ownerUserName);
?>
# The IP address of this machine has been auto-detected.
# This site will need to be verified again if the IP address changes.
<VirtualHost <?php print $ipAddress ?>:<?php print $site_port; ?>>
  ServerName   <?php print $site_url; ?>

<?php
  if (is_array($aliases)) {
    foreach ($aliases as $alias_url) {
      if (trim($alias_url)) {
?>
  ServerAlias <?php print $alias_url; ?>
<?php
      }
    }
  }
?>

  UseCanonicalName Off

  DocumentRoot <?php print $publish_path; ?>

<?php if ($isPlesk) {?>

  # MPM-ITK Username
  AssignUserID <?php print $ownerUserName ?> psacln

  # Plesk per-client logging
  CustomLog  <?php print $pleskVhostPath; ?>/statistics/logs/access_log plesklog
  ErrorLog  <?php print $pleskVhostPath; ?>/statistics/logs/error_log

  # Plesk web users
  <IfModule mod_userdir.c>
    UserDir /var/www/vhosts/<?php print $site_url; ?>/web_users
  </IfModule>

  <Directory /var/www/vhosts/<?php print $site_url; ?>/web_users>
    <IfModule sapi_apache2.c>
      php_admin_flag engine off
    </IfModule>

    <IfModule mod_php5.c>
      php_admin_flag engine off
    </IfModule>
  </Directory>
<?php
  }
?>

  <IfModule mod_ssl.c>
    SSLEngine off
  </IfModule>

  RewriteEngine on

  # Disabled site -- redirect all requests
  # the ? at the end is to remove any query string in the original url
  RewriteRule ^(.*)$ <?php print $redirect_url . '/' . $site_url ?>?
</VirtualHost>