<?php
  drush_plesk_get_plesk_info_for_template(
                                    $site_url,
                                    $ipAddress,
                                    $isPlesk,
                                    $pleskVhostPath,
                                    $publish_path,
                                    $ownerUserName);
?>
# The IP address of this machine has been auto-detected.
# This site will need to be verified again if the IP address changes.
<VirtualHost <?php print $ipAddress ?>:<?php print $site_port; ?>>
  ServerName  <?php print $site_url; ?>

<?php
  if (!$redirection && is_array($aliases)) {
    foreach ($aliases as $alias_url) {
      if (trim($alias_url)) {
?>
  ServerAlias <?php print $alias_url; ?>

<?php
      }
    }
  }
?>

<?php if ($site_mail) { ?>
  ServerAdmin <?php print $site_mail; ?>

<?php } ?>

  UseCanonicalName Off

  DocumentRoot <?php print $publish_path; ?>

<?php if ($isPlesk) {?>

  # MPM-ITK Username
  AssignUserID <?php print $ownerUserName ?> psacln

  # Plesk per-client logging
  CustomLog  <?php print $pleskVhostPath; ?>/statistics/logs/access_log plesklog
  ErrorLog  <?php print $pleskVhostPath; ?>/statistics/logs/error_log

  # Plesk web users
  <IfModule mod_userdir.c>
    UserDir /var/www/vhosts/<?php print $site_url; ?>/web_users
  </IfModule>

  <Directory /var/www/vhosts/<?php print $site_url; ?>/web_users>
    <IfModule sapi_apache2.c>
      php_admin_flag engine off
    </IfModule>

    <IfModule mod_php5.c>
      php_admin_flag engine off
    </IfModule>
  </Directory>
<?php
  }
?>

  <IfModule mod_ssl.c>
    SSLEngine off
  </IfModule>

  <Directory <?php print $publish_path; ?>>
    <IfModule sapi_apache2.c>
      php_admin_flag engine on
      php_admin_flag safe_mode off
      php_admin_value open_basedir none
    </IfModule>

    <IfModule mod_php5.c>
      php_admin_flag engine on
      php_admin_flag safe_mode off
      php_admin_value open_basedir none
    </IfModule>

      Options -Includes -ExecCGI
  </Directory>

  <?php print $extra_config; ?>

  # Error handler for Drupal > 4.6.7
  <Directory "<?php print $publish_path; ?>/sites/<?php print trim($site_url, '/'); ?>/files">
    # Prevent PHP files in the "files/" folder from being executable.
    SetHandler This_is_a_Drupal_security_line_do_not_remove
  </Directory>
</VirtualHost>
