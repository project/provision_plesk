<?php
  drush_plesk_get_plesk_info_for_template(
                                    $site_url,
                                    $ipAddress,
                                    $isPlesk,
                                    $pleskVhostPath,
                                    $publish_path,
                                    $ownerUserName);
?>
# The IP address of this site has been auto-detected.
# This site will need to be verified again if the IP address changes.
<VirtualHost <?php print $ipAddress ?>:<?php print $site_port; ?>>
<?php
  if (is_array($aliases) && count($aliases)) {
?>
  ServerName <?php print array_pop($aliases); ?>

<?php
  }

  if (count($aliases)) {
?>
  ServerAlias <?php print join(" ", $aliases); ?>

<?php
  }
  else {
    # this should never happen and has the potential of creating an infinite redirection loop
?>
  ServerName <?php print $site_url ?>

<?php
  }

  if ($site_mail) {
?>
  ServerAdmin <?php print $site_mail; ?>

<?php
  }
?>

  UseCanonicalName Off

<?php if ($isPlesk) {?>

  # MPM-ITK Username
  AssignUserID <?php print $ownerUserName ?> psacln

  # Plesk per-client logging
  CustomLog  <?php print $pleskVhostPath; ?>/statistics/logs/access_log plesklog
  ErrorLog  <?php print $pleskVhostPath; ?>/statistics/logs/error_log

  # Plesk web users
  <IfModule mod_userdir.c>
    UserDir /var/www/vhosts/<?php print $site_url; ?>/web_users
  </IfModule>

  <Directory /var/www/vhosts/<?php print $site_url; ?>/web_users>
    <IfModule sapi_apache2.c>
      php_admin_flag engine off
    </IfModule>

    <IfModule mod_php5.c>
      php_admin_flag engine off
    </IfModule>
  </Directory>
<?php
  }
?>

  <IfModule mod_ssl.c>
    SSLEngine off
  </IfModule>

  RewriteEngine on

<?php
  if ($ssl_redirect) {
?>
    RedirectMatch permanent ^(.*) https://<?php print $site_url ?>$1
<?php
  }
  else {
?>
    RedirectMatch permanent ^(.*) http://<?php print $site_url ?>$1
<?php
  }
?>
</VirtualHost>