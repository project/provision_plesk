<?php
/**
 * @file
 *  Logic for the "plesk-reset-pf-perm" command of the Drush Plesk module.
 *
 * @author
 *   Guy Paddock (guy.paddock@redbottledesign.com)
 */

  /**
   * Implementation of hook_drush_plesk_reset_pf_perm().
   *
   * Resets all of the permissions and ownership of the current Drupal install to default values appropriate for the
   * current Aegir and Plesk environment. This is useful if the current permissions are causing clients to have trouble
   * accessing their Drupal sites.
   *
   * This command requires aegir to have sudo access for the "aegir_chown" or "chown" command.
   *
   * One unfortunate side-effect of this command is that all sites under the sites/ folder lose their current owners.
   * Therefore, the user has to manually restore ownership to clients after running this command.
   */
  function drush_drush_plesk_plesk_reset_pf_perm() {
    _drush_plesk_reset_platform_permissions();

    if (!drush_get_error()) {
      drush_log(
        dt("\nPermissions have been reset on the Drupal install in %path.\n\n" .
           "NOTE: All sites under the sites/ folder currently belong to %user:%group.\n" .
           "You will need to restore appropriate ownership to clients.",
           array('%path' => $platformPath, '%user' => $aegirUser, '%group' => $aegirGroup)),
        'success');
    }
  }

  /**
   * Reset all of the permissions and ownership of the current Drupal install to default values appropriate for the
   * current Aegir and Plesk environment.
   *
   * If an error occurs, the appropriate message will be set through drush_set_error().
   */
  function _drush_plesk_reset_platform_permissions() {
    $aegirUser    = drush_get_option('script_user');
    $aegirGroup   = $aegirUser;
    $webGroup     = drush_get_option('web_group');
    $platformPath = drush_get_option('publish_path');

    // Make sure only Aegir owns the Drupal install.
    ownership_utils_sudo_chown_logged(
      $aegirUser,
      $aegirGroup,
      $platformPath,
      TRUE,
      'Successfully changed owner of Drupal platform in %path to %user:%group.',
      'Failed to change owner of Drupal platform in %path to %user:%group.');

    // Make sure Aegir can modify the Drupal install.
    provision_path(
      'chmod_recursive',
      $platformPath,
      0755,
      "Changed permissions of @path to @confirm",
      "Could not change permissions of @path to @confirm",
      'DRUSH_PERM_ERROR');

    $sitesFolder = "$platformPath/sites";

    // Make sure that both Aegir and web group own the entire "sites/" folder.
    ownership_utils_sudo_chown_logged(
      $aegirUser,
      $webGroup,
      $sitesFolder,
      TRUE,
      'Successfully changed owner of %path to %user:%group.',
      'Failed to change owner of %path to %user:%group.');

    // Change the mode on the "files" folder of all site folders to 2770
    foreach (scandir($sitesFolder) as $siteFolder) {
      $siteFileFolderPath = "$sitesFolder/$siteFolder/files";

      if (!in_array($siteFolder, array('.', '..')) && is_dir($siteFileFolderPath)) {
        provision_path(
          'chmod_recursive',
          $siteFileFolderPath,
          02770,
          "Changed permissions of @path to @confirm",
          "Could not change permissions of @path to @confirm",
          'DRUSH_PERM_ERROR');
      }
    }

    // Change permission on all *files* (not directories) in the platform to 644, so they're not shell-executable
    ownership_utils_recursive_chmod_files($platformPath, 0644);

    // Make drushrc.php files readable only by owner
    ownership_utils_recursive_chmod_files($platformPath, 0400, 'drushrc.php');

    // Make settings.php files readable only by owner and group
    ownership_utils_recursive_chmod_files($platformPath, 0440, 'settings.php');
  }