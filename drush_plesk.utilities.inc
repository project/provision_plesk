<?php
/**
 * @file
 *  Generic Plesk-related utility functions.
 *
 * @author
 *   Guy Paddock (guy.paddock@redbottledesign.com)
 */
  /**
   * Obtain the information necessary to properly generate a virtual host configuration for a site that may or may not
   * be maintained by Plesk.
   *
   * TODO: Make work for sub-domains.
   *
   * @param $site_url
   *        The URL of the site (i.e. "example.com", "aegir.redbottledesign.com").
   *        This will be the same name as the folder for the site under the "sites/" folder.
   *
   * @param $ipAddress
   *        An output parameter specifying the IP address that the site is being hosted on.
   *
   * @param $pleskMaintained
   *        An output parameter specifying whether or not the site is maintained by Parallels Plesk.
   *
   * @param $pleskVhostPath
   *        If $pleskMaintained is TRUE, this output parameter will be updated to reflect the home path that Plesk has
   *        specified for the site. If $pleskMaintained is FALSE, any value passed-in for this parameter will not be
   *        changed.
   *
   * @param $publish_path
   *        If $pleskMaintained is TRUE, this output parameter will be updated to reflect the publish path that Plesk
   *        uses for the site. If $pleskMaintained is FALSE, any value passed-in for this parameter will not be changed.
   *
   * @param $ownerUserName
   *        If $pleskMaintained is TRUE, this output parameter will be updated to reflect the POSIX user name of the
   *        account that owns the folder indicated for $publish_path. If $pleskMaintained is FALSE, any value passed-in
   *        for this parameter will not be changed.
   */
  function drush_plesk_get_plesk_info_for_template($site_url, &$ipAddress, &$pleskMaintained, &$pleskVhostPath,
                                                   &$publish_path, &$ownerUserName) {
    $pleskVhostPath = "/var/www/vhosts/$site_url";

    // If Plesk has already created a VHost folder for this site, use it and configure the site appropriately.
    if (file_exists($pleskVhostPath) && is_dir($pleskVhostPath)) {
      $pleskMaintained  = TRUE;

      $publish_path     = $pleskVhostPath . "/httpdocs";
      $ipAddress        = drush_plesk_db_get_ip_for_domain($site_url);

      // Determine which client owns the folder
      $ownerUserName    = provision_path_owner($publish_path);
    }
    else {
      $pleskMaintained  = FALSE;
      $ipAddress        = drush_get_option('web_ip', '*');
    }
  }

  /**
   * A utility function for obtaining the IP address associated with the specified network interface.
   * This is useful for IP-based virtual hosts.
   *
   * @param   $interface
   *          The name of the network interface (i.e. "eth0", "venet0:0", "wlan0", etc.) for which an IP address
   *          is desired. If not specified, the default is "venet0:0", which is the default value for the first
   *          interface of MediaTemple (dv) installations of Plesk and Virtuozzo.
   *
   * @return  The IP address associated with the specified network interface, or asterisk if the IP address of the
   *          interface cannot be determined.
   */
  function drush_plesk_get_ip_address_for_interface($interface = 'venet0:0') {
    // Default to wildcard.
    $ipAddress    = '*';

    $returnVal    = 0;
    $outputLines  = array();

    @exec('/sbin/ifconfig ' . $interface, $outputLines, $returnVal);

    if ($returnVal == 0) {
      foreach ($outputLines as $line) {
        $matches = array();

        // Capture the IP address information.
        if (preg_match('/inet addr\:([0-9]{3}\.[0-9]{3}\.[0-9]{3}\.[0-9]{3})/', $line, $matches) > 0) {
          $ipAddress = $matches[1];
          break;
        }
      }
    }

    return $ipAddress;
 }