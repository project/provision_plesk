#!/usr/bin/php
<?php
/**
 * @file
 *    A wrapper around <code>chown</code> that allows Aegir to change ownership of files and folders, while reducing the
 *    area of effect if the Aegir account is compromised. This script only allows a <code>chown</code> operation to
 *    succeed if it is being performed on files and folders within an Aegir platform.
 *
 *    Copy this file to "/usr/local/sbin/", rename it "aegir_chown", and mark it executable.
 *
 * @author
 *    Guy Paddock (guy.paddock@redbottledesign.com)
 */

  /**
   * A constant that defines the "safe paths" for <code>chown</code> operations. Any <code>chown</code> operations that
   * occur within these folders or their sub-folders will be allowed to proceed, while <code>chown</code> operations
   * outside these folders will be denied.
   *
   * TODO: Find out if there is a way to determine this automatically (for example, base it off the home folder of the
   *       "aegir" user account.
   *
   * @var string
   */
  $SAFE_PATHS = array(
    '/var/aegir/platforms',
    '/var/www/vhosts/'
  );

  // MAIN
  if ($argc < 3) {
    fwrite(STDERR, "This script is not intended to be run directly.\n");
    exit(1);
  }

  else {
    // Skip script filename argument
    array_shift($argv);

    if (commandIsSecure($argv)) {
      invokeChown($argv);
    }
    else {
      fwrite(STDERR, "Permission denied.\n");
      exit(2);
    }
  }
?>
<?php
  /**
   * Determine if the <code>chown</code> command with the specified command-line arguments is secure and should be
   * allowed to proceed.
   *
   * A command is considered insecure if it references files that are outside the safe path (i.e. Aegir's platform
   * folders).
   *
   * @param   $arguments
   *          The command-line arguments to verify for security.
   *
   * @return  <code>TRUE</code> if the arguments are secure and should be passed-on to an underlying <code>chown</code>
   *          command, <code>FALSE</code> otherwise.
   */
  function commandIsSecure($arguments) {
    $isSecure     = TRUE;

    $recursive    = FALSE;
    $userAndGroup = NULL;
    $files        = array();

    // Parse arguments and populate local vars
    parseArguments($arguments, $recursive, $userAndGroup, $files);

    foreach ($files as $filename) {
      if (!fileIsInSafePath($filename)) {
        $isSecure = FALSE;
        break;
      }
    }

    return $isSecure;
  }

  /**
   * Parses the command-line arguments that were passed to this script.
   *
   * @param $arguments
   *        The array of arguments to parse, not including the program name.
   *
   * @param $recursive
   *        Output parameter that indicates whether or not the <code>chown</code> operation is recursive.
   *
   * @param $userAndGroup
   *        Output parameter that indicates the user and/or group that is being given ownership of the file(s).
   *
   * @param $files
   *        Output parameter that indicates the files that the command is being performed on.
   */
  function parseArguments(array $arguments, &$recursive, &$userAndGroup, &$files) {
    $userAndGroup = NULL;

    foreach ($arguments as $argument) {
      if ($argument == '-R') {
        $recursive  = TRUE;
      }
      else {
        if ($userAndGroup == NULL) {
          $userAndGroup = $argument;
        }
        else {
          $files[] = $argument;
        }
      }
    }
  }

  /**
   * Invokes <code>chown</code> with the specified command-line arguments.
   *
   * The output and return value of the command is passed through to the calling process.
   *
   * @param $arguments
   *        The arguments to pass to the underlying <code>chown</code> command.
   */
  function invokeChown(array $arguments) {
    $argString  = implode(' ', $arguments);
    $result     = 0;

    // Add --no-dereference to prevent trickery with symlinks
    passthru('chown --no-dereference ' . $argString, $result);

    // Pass through result
    exit($result);
  }

  /**
   * Determines whether the specified file lies within a path that is in the list of safe paths.
   *
   * @param   $filename
   *          The filename of the file that will be checked.
   *
   * @return  <code>TRUE</code> if the specified file lies within a path that is in the list of safe paths,
   *          <code>FALSE</code> otherwise.
   */
  function fileIsInSafePath($filename) {
    global $SAFE_PATHS;

    $inSafePath   = FALSE;
    $fullFileName = realpath($filename);

    foreach ($SAFE_PATHS as $currentPath) {
      $currentPathLen = strlen($currentPath);

      if ((strlen($fullFileName) > $currentPathLen) &&
          (substr($fullFileName, 0, $currentPathLen) == $currentPath)) {
        $inSafePath = TRUE;
        break;
      }
    }

    return $inSafePath;
  }
?>