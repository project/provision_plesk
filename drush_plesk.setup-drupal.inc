<?php
/**
 * @file
 *  Logic for the "plesk-setup-drupal" command of the Drush Plesk module.
 *
 * @author
 *   Guy Paddock (guy.paddock@redbottledesign.com)
 */
  /**
   * A constant for the name of document root folders for clients.
   *
   * @var string
   */
  define('DP_SD_DOCROOT', 'httpdocs');

  /**
   * Implementation of hook_drush_plesk_setup_drupal_validate().
   *
   * Validates the document root path that the user has specified to ensure that it is valid, which means that it
   * meets the following criteria:
   *  - The folder actually exists.
   *  - The folder is named "httpdocs" (or whatever is appropriate for the current environment, specified by the
   *    DP_SD_DOCROOT constant).
   *
   * These checks are to ensure that the user doesn't specify the wrong folder and end up bashing something.
   *
   * @param $doc_root
   *        The path the user has specified for the document root folder.
   */
  function drush_drush_plesk_plesk_setup_drupal_validate($doc_root = NULL) {
    $currentUser = exec('whoami');

    if (empty($doc_root)) {
      drush_set_error(dt("Please specify the target document root."));
    }
    elseif (!is_dir($doc_root)) {
      drush_set_error(
        dt('The specified document root (%path) does not exist, is not accessible, or is not a directory.',
           array('%path' => $doc_root)));
    }
    elseif (substr(realpath($doc_root), -strlen(DP_SD_DOCROOT)) != DP_SD_DOCROOT) {
      drush_set_error(
        dt('The specified document root (%path) is not a document root directory (i.e. "%example" folder).',
           array('%path' => $doc_root, '%example' => DP_SD_DOCROOT)));
    }
  }

  /**
   * Implementation of hook_drush_plesk_setup_drupal().
   *
   * Set-up the specified client's document root folder to access the current Drupal site. This is done by means of
   * symlinks to the shared Drupal platform/install; the Drupal site remains in its current location to allow it to be
   * maintained by Aegir.
   *
   * This command requires aegir to have sudo access for the "aegir_chown" or "chown" command, because it must
   * temporarily take ownership of the document root, the user's home folder, and the Drupal site folder while it runs.
   *
   * @param $doc_root
   *        The validated path to the document root folder.
   */
  function drush_drush_plesk_plesk_setup_drupal($doc_root = NULL) {
    $platformPath = drush_get_option('publish_path');
    $aegirUser    = drush_get_option('script_user');
    $webGroup     = drush_get_option('web_group');

    $siteUrl      = drush_get_option('site_url');
    $siteOwner    = provision_path_owner($doc_root);

    _drush_plesk_setup_drupal_create_platform_symlink($doc_root, $platformPath, $aegirUser, $webGroup);
    _drush_plesk_setup_drupal_create_drupal_files($doc_root, $platformPath, $aegirUser, $webGroup, $siteUrl,
                                                   $siteOwner);
    _drush_plesk_setup_drupal_correct_site_ownership($platformPath, $aegirUser, $webGroup, $siteUrl, $siteOwner);

    drush_log(
      dt('The set-up of the Drupal site %site in %doc_root is now complete.',
         array('%site' => $siteUrl, '%doc_root' => $doc_root)),
         'success');
  }

  /**
   * Creates a symbolic link called "drupal_platform" in the folder that contains the document root.
   *
   * Thes link that is created points to the absolute path of the Drupal platform that runs the site. All other symbolic
   * links created by this command are relative to the "drupal_platform" symbolic link, to make it easy to change
   * platforms (to facilitate migration).
   *
   * @param $doc_root
   *        The validated path to the document root folder.
   *
   * @param $platformPath
   *        The path to the Drupal platform that the site runs on.
   *
   * @param $aegirUser
   *        The POSIX user name for the account that Aegir runs under.
   *
   * @param $webGroup
   *        The POSIX group that the web server is a member of.
   */
  function _drush_plesk_setup_drupal_create_platform_symlink($doc_root, $platformPath, $aegirUser, $webGroup) {
    $ownerHome    = dirname(realpath($doc_root));

    // Temporarily take ownership of the user's home folder so we can create the platform symlink.
    ownership_utils_sudo_chown_logged(
      $aegirUser,
      $webGroup,
      $ownerHome,
      FALSE,
      "Successfully took temporary ownership of user's home folder (%path).",
      "Failed to take temporary ownership of user's home folder (%path).");

    // Ensure permissions are correct on the folder
    provision_path(
      'chmod',
      $ownerHome,
      0755,
      "Changed permissions of user's home folder (@path) to @confirm",
      "Could not change permissions of user's home folder (@path) to @confirm",
      'DRUSH_PERM_ERROR');

    _drush_plesk_setup_drupal_create_or_update_symlink($platformPath, "$ownerHome/drupal_platform");

    // Return ownership of the user's home folder
    ownership_utils_sudo_chown_logged(
      'root',
      'root',
      $ownerHome,
      FALSE,
      "Successfully restored ownership of user's home folder (%path) to %user.",
      "Failed to restore ownership of user's home folder (%path) to %user.");
  }

  /**
   * Creates all of the necessary files and symbolic links in the document root for Drupal to run from the shared Drupal
   * install.
   *
   * All symbolic links are created relative to the "drupal_platform" symbolic link that is present in the parent
   * folder.
   *
   * @param $doc_root
   *        The validated path to the document root folder.
   *
   * @param $platformPath
   *        The path to the Drupal platform that the site runs on.
   *
   * @param $aegirUser
   *        The POSIX user name for the account that Aegir runs under.
   *
   * @param $webGroup
   *        The POSIX group that the web server is a member of.
   *
   * @param $siteUrl
   *        The URL of the site (i.e. "example.com", "aegir.redbottledesign.com").
   *        This will be the same name as the folder for the site under the "sites/" folder.
   *
   * @param $siteOwner
   *        The POSIX user name of the user who owns the site.
   */
  function _drush_plesk_setup_drupal_create_drupal_files($doc_root, $platformPath, $aegirUser, $webGroup,
                                                          $siteUrl, $siteOwner) {
    $originalDir  = getcwd();

    // Temporarily take ownership of the document root so we can create the Drupal files.
    ownership_utils_sudo_chown_logged(
      $aegirUser,
      $webGroup,
      $doc_root,
      TRUE,
      "Successfully took temporary ownership of document root folder (%path).",
      "Failed to take temporary ownership of document root folder (%path)."
      );

    // Ensure permissions are correct on the folder
    provision_path(
      'chmod',
      $doc_root,
      0750,
      "Changed permissions of document root folder (@path) to @confirm",
      "Could not change permissions of document root folder (@path) to @confirm",
      'DRUSH_PERM_ERROR');

    chdir($doc_root);

    // Remove write for others and group.
    umask(027);

    _drush_plesk_setup_drupal_copy_platform_file(".htaccess", $platformPath);
    _drush_plesk_setup_drupal_copy_platform_file("robots.txt", $platformPath);

    _drush_plesk_setup_drupal_create_or_update_symlink("../drupal_platform/index.php");
    _drush_plesk_setup_drupal_create_or_update_symlink("../drupal_platform/update.php");
    _drush_plesk_setup_drupal_create_or_update_symlink("../drupal_platform/cron.php");
    _drush_plesk_setup_drupal_create_or_update_symlink("../drupal_platform/xmlrpc.php");
    _drush_plesk_setup_drupal_create_or_update_symlink("../drupal_platform/includes/");
    _drush_plesk_setup_drupal_create_or_update_symlink("../drupal_platform/misc/");
    _drush_plesk_setup_drupal_create_or_update_symlink("../drupal_platform/modules/");
    _drush_plesk_setup_drupal_create_or_update_symlink("../drupal_platform/themes/");

    if (!is_dir('./sites')) {
      provision_path(
        'mkdir',
        './sites',
        TRUE,
        'Successfully created @path directory.',
        'Failed to create @path directory.');
    }

    _drush_plesk_setup_drupal_create_or_update_symlink("../../drupal_platform/sites/all/",     "./sites/all");
    _drush_plesk_setup_drupal_create_or_update_symlink("../../drupal_platform/sites/$siteUrl", "./sites/$siteUrl");

    // Give ownership of document root back to site owner
    ownership_utils_sudo_chown_logged(
      $siteOwner,
      $webGroup,
      $doc_root,
      TRUE,
      'Successfully restored owner of document root folder (%path) to %user:%group.',
      'Failed to restore owner of document root folder (%path) to %user:%group.');

    chdir($originalDir);
  }

  /**
   * Ensures that the folder for the site under the "sites/" folder of the platform belongs to the site owner and
   * has the correct permissions.
   *
   * @param $platformPath
   *        The path to the Drupal platform that the site runs on.
   *
   * @param $aegirUser
   *        The POSIX user name for the account that Aegir runs under.
   *
   * @param $webGroup
   *        The POSIX group that the web server is a member of.
   *
   * @param $siteUrl
   *        The URL of the site (i.e. "example.com", "aegir.redbottledesign.com").
   *        This will be the same name as the folder for the site under the "sites/" folder.
   *
   * @param $siteOwner
   *        The POSIX user name of the user who owns the site.
   */
  function _drush_plesk_setup_drupal_correct_site_ownership($platformPath, $aegirUser, $webGroup, $siteUrl,
                                                             $siteOwner) {
    $siteFolder = "$platformPath/sites/$siteUrl/";

    // Temporarily take ownership of the site folder so we can change its mode.
    ownership_utils_sudo_chown_logged(
      $aegirUser,
      $webGroup,
      $siteFolder,
      TRUE,
      'Successfully changed owner of %path to %user:%group.',
      'Failed to change owner of %path to %user:%group.');

    provision_path(
      'chmod_recursive',
      $siteFolder,
      0750,
      "Changed permissions of site @path to @confirm",
      "Could not change permissions of site @path to @confirm",
      'DRUSH_PERM_ERROR');

    // Change permission on all *files* (not directories) in the site to 640, so they're not shell-executable
    ownership_utils_recursive_chmod_files($siteFolder, 0640);

    // Give ownership of site to site owner
    ownership_utils_sudo_chown_logged(
      $siteOwner,
      $webGroup,
      $siteFolder,
      TRUE,
      'Successfully changed owner of %path to %user:%group.',
      'Failed to change owner of %path to %user:%group.');
  }

  /**
   * A utility method for copying the specified file from the specified platform folder into the current working
   * directory.
   *
   * @param $platformFolder
   *        The platform folder from which the file will be copied.
   *
   * @param $fileName
   *        The name of the file to copy.
   */
  function _drush_plesk_setup_drupal_copy_platform_file($fileName, $platformFolder) {
    if (!file_exists($fileName)) {
      if (copy("$platformFolder/$fileName",  "./$fileName")) {
        drush_log(
          dt("Successfully copied %file from the %platform platform.",
             array('%file' => $fileName, '%platform' => $platformFolder)));
      }
      else {
        drush_set_error(
          'DRUSH_PERM_ERROR',
          dt("Failed to copy %file from the %platform platform.",
             array('%file' => $fileName, '%platform' => $platformFolder)));
      }
    }
    else {
       drush_log(
          dt("The %file already exists and will not be replaced with the copy from the %platform platform.",
             array('%file' => $fileName, '%platform' => $platformFolder)));
    }
  }

  /**
   * A utility method for creating a symbolic link to the specified target, or updating the existing symlink if it
   * already exists and does not already point to the correct path.
   *
   * @param $target
   *        The file or folder to which the symlink should point.
   *
   * @param $link
   *        An optional parameter to specify the name of the symbolic link to create. If omitted, the name of the
   *        symbolic link will be created in the current working directory, with the same as the name of the target file
   *        or folder.
   */
  function _drush_plesk_setup_drupal_create_or_update_symlink($link_target, $link = NULL) {
    // Allow the link path to be based on the target path.
    if ($link == NULL) {
      $link = basename($link_target);
    }

    if (file_exists($link) && is_link($link) && (readlink($link) != $link_target)) {
      provision_path(
        'unlink',
        $link,
        TRUE,
        "Removed existing, stale symbolic link at @path.",
        "Failed to remove existing, stale symbolic link at @path.");
    }

   drush_log(dt('Symbolically linking %path to %target', array('%path' => $link, '%target' => $link_target)));

   provision_path(
      'symlink',
      $link_target,
      $link,
      "Successfully created symbolic link to @path.",
      "Failed to create symbolic link to @path.");
  }