<?php
/**
 * @file
 *   Logic for querying and updating the Plesk database.
 *
 * @author
 *   Guy Paddock (guy.paddock@redbottledesign.com)
 */

  /**
   * The database Plesk uses for storing site information.
   *
   * @var string
   */
  define('DP_DD_PLESK_DATABASE', 'psa');

  /**
   * Connect to Provision's master database.
   *
   * In a standard shared VPS set-up, this should be the same database server that Plesk uses.
   */
  function drush_plesk_db_connect_to_master_database() {
    // Taken from provision_mysql_drush_init()
    drush_set_default('master_db', $GLOBALS['db_url']);

    $master_db  = drush_get_option('master_db');
    $db         = parse_url($master_db);

    drush_set_default('master_db_user', urldecode($db['user']));
    drush_set_default('master_db_passwd', urldecode($db['pass']));

    drush_set_default('master_db_host', urldecode($db['host']));
    drush_set_default('db_host', urldecode($db['host']));

    drush_set_default('master_db_type', $db['scheme']);
    drush_set_default('db_type', $db['scheme']);

    provision_db_connect();
  }

  /**
   * Queries the Plesk database to find the IP address that corresponds to the specified domain name, if the domain
   * is managed by Plesk.
   *
   * @param $domain
   *        The domain name for which an IP address is desired.
   *
   * @return The IP address on which Plesk has configured the specified domain; or FALSE if the specified domain
   *          does not exist on the current machine and/or is not maintained by Plesk.
   */
  function drush_plesk_db_get_ip_for_domain($domain) {
    $ipAddress  =
      provision_db_result(
        provision_db_query(
          "SELECT `address`.`ip_address` ".
          "FROM `psa`.`domains` as `domain` ".

          "JOIN `psa`.`hosting` ".
          "ON `psa`.`hosting`.`dom_id` = `domain`.`id` ".

          "JOIN `psa`.`IP_Addresses` as `address` ".
          "ON `psa`.`address`.`id` = `hosting`.`ip_address_id` ".

          "WHERE `psa`.`domain`.`name` = '%s'",
          $domain));

    return $ipAddress;
  }

  /**
   * Look-up the Plesk database record that corresponds to the specified Drupal site and database, and return its
   * identifier. If no record of the database exists in the database, a new record will be created, and the identifier
   * for the new record will be returned.
   *
   * A connection to the Plesk database must already be open before calling this method.
   *
   * If the provided site is not currently managed by Plesk, the appropriate error message will be raised with
   * drush_set_error(), and NULL will be returned.
   *
   * @param $siteUrl
   *        The URL of the site (i.e. "example.com", "aegir.redbottledesign.com").
   *        This will be the same name as the folder for the site under the "sites/" folder.
   *
   * @param $databaseName
   *        The name of the database for which information is desired.
   *
   * @return The numeric identifier for the database record that corresponds to the specified database, or NULL if
   *         the specified site is not currently being maintained by Plesk.
   */
  function drush_plesk_db_get_or_insert_database_info($siteUrl, $databaseName) {
    $pleskDbId      = NULL;

    $pleskDomainId  =
      provision_db_result(
        provision_db_query(
          "SELECT `domain`.`id` ".
          "FROM `psa`.`domains` as `domain` ".

          "WHERE `domain`.`name` = '%s'",
          $siteUrl));

    if ($pleskDomainId === FALSE) {
      drush_set_error(dt('Plesk does not appear to be managing the site "%site".', array('%site' => $siteUrl)));
    }
    else {
      $pleskDbId =
          provision_db_result(
            provision_db_query(
              "SELECT `db`.`id` ".
              "FROM `psa`.`data_bases` as `db` ".

              "JOIN `psa`.`domains` as `domain` ".
              "ON `domain`.`id` = `db`.`dom_id`".

              "WHERE `db`.`dom_id` = %d AND `db`.`name` = '%s'",
              $pleskDomainId, $databaseName));

      if ($pleskDbId === FALSE) {
        $insertResult =
          provision_db_query(
            "INSERT INTO `psa`.`data_bases` (`name`, `dom_id`, `db_server_id`) VALUES ('%s', %d, %d)",
            $databaseName,
            $pleskDomainId,
            1); // FIXME: Currently limited to just one database server

        if ($insertResult === FALSE) {
          drush_set_error(
            dt('Failed to add database "%database" to the Plesk "data_bases" table for Plesk domain ID %domain.',
               array('%database' => $databaseName, '%domain' => $pleskDomainId)));
        }
        else {
          $pleskDbId = mysql_insert_id();

          drush_log(
            dt('Successfully added database information for "%db" to Plesk "data_bases" table.',
               array('%db' => $databaseName)));
        }
      }
      else {
        drush_log(
          dt('Database information for "%db" already exists in Plesk "data_bases" table.',
             array('%db' => $databaseName)));
      }
    }

    return $pleskDbId;
  }

  /**
   * Look-up the Plesk database user account that corresponds to the specified database and log-in name and return its
   * identifier. If no record of the user account exists in the database, a new record will be created, and the
   * identifier for the new record will be returned.
   *
   * A connection to the Plesk database must already be open before calling this method.
   *
   * @param $pleskDbId
   *        The primary key identifier for the database to which the desired account pertains.
   *
   * @param $databaseUser
   *        The log-in / user name for the desired database user account.
   *
   * @param $databasePassword
   *        The password for the desired database user account. This parameter is not used to locate the account, but
   *        its value will be used if the account does not exist and has to be added to the database.
   *
   * @return The numeric identifier for the database record that corresponds to the specified database user, or FALSE
   *         if Plesk is
   */
  function drush_plesk_db_get_or_insert_database_user($pleskDbId, $databaseUser, $databasePassword) {
    $pleskDbUserId =
      provision_db_result(
        provision_db_query("SELECT `id` FROM `psa`.`db_users` WHERE `login`='%s'", $databaseUser));

    if ($pleskDbUserId === FALSE) {
      $pleskDbUserId  = drush_plesk_db_insert_database_user($pleskDbId, $databaseUser, $databasePassword);
    }
    else {
      drush_log(dt('Database user "%user" already exists in Plesk "db_users" table.', array('%user' => $databaseUser)));
    }

    return $pleskDbUserId;
  }

  /**
   * Add information on the specified database user account to the Plesk database.
   *
   * A connection to the Plesk database must already be open before calling this method.
   *
   * If information about the specified user account cannot be added to the Plesk database, the appropriate error
   * message will be raised with drush_set_error().
   *
   * @param $pleskDbId
   *        The primary key identifier for the database to which the account should pertain.
   *
   * @param $databaseUser
   *        The log-in / user name for the database user account.
   *
   * @param $databasePassword
   *        The password for the database user account.
   */
  function drush_plesk_db_insert_database_user($pleskDbId, $databaseUser, $databasePassword) {
    $pleskDbUserId = NULL;

    $accountInsertResult =
      provision_db_query("INSERT INTO `psa`.`accounts` (`password`) VALUES ('%s')", $databasePassword);

    if ($accountInsertResult === FALSE) {
      drush_set_error(
        dt('Failed to add password for database user "%user" to the Plesk "accounts" table.',
           array('%user' => $databaseUser)));
    }
    else {
      drush_log(
        dt('Successfully added password for database user "%user" to Plesk "accounts" table.',
           array('%user' => $databaseUser)));

      $pleskAccountId = mysql_insert_id();

      $userInsertResult =
        provision_db_query(
          "INSERT INTO `psa`.`db_users` (`login`, `account_id`, `db_id`) VALUES ('%s', %d, %d)",
          $databaseUser,
          $pleskAccountId,
          $pleskDbId);

      if ($userInsertResult === FALSE) {
        drush_set_error(
          dt('Failed to add user %user to the Plesk "db_users" table for Plesk account ID %account and database ID %db.',
             array('%user' => $databaseUser, '%account' => $pleskAccountId, '%db' => $pleskDbId)));
      }
      else {
        $pleskDbUserId = mysql_insert_id();

        drush_log(
          dt('Successfully added database user "%user" to Plesk "accounts" table.', array('%user' => $databaseUser)));
      }
    }

    return $pleskDbUserId;
  }

  /**
   * Updates the information in the Plesk database to make the specified database user account the default account for
   * the specified database.
   *
   * A connection to the Plesk database must already be open before calling this method.
   *
   * If information about the specified user account cannot be added to the Plesk database, the appropriate error
   * message will be raised with drush_set_error().
   *
   * @param $pleskDbId
   *        The primary key identifier for the database for which information will be updated.
   *
   * @param $pleskDbUserId
   *        The primary key of the database user account that will be made the default account for the database.
   */
  function drush_plesk_db_update_database_default_user($pleskDbId, $pleskDbUserId) {
    $result =
      provision_db_query(
        "UPDATE `psa`.`data_bases` ".
        "SET `default_user_id` = %d ".
        "WHERE `id` = %d",
        $pleskDbUserId,
        $pleskDbId);

    if (!$result) {
      drush_set_error(
        dt('Failed to update the "default_user_id" column of the Plesk "data_bases" table to %user_id for database ID '.
           '%database_id.',
           array('%user_id' => $pleskDbUserId, '%database_id' => $pleskDbId)));
    }
  }